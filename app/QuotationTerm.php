<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationTerm extends Model
{
    protected $fillable = ['description'];
}
