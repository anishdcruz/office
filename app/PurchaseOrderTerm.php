<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderTerm extends Model
{
    protected $fillable = ['description'];
}
