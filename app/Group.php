<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['owner_id', 'group_id', 'name'];

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function members()
    {
        return $this->hasMany(GroupMember::class);
    }

    public function pending()
    {
        return $this->hasMany(Invitation::class);
    }
}
