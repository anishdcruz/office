<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $fillable = [
        'client_id', 'quotation_no', 'title',
        'quotation_date', 'internal_note',
        'discount', 'in_words', 'group_id', 'user_id',
        'grand_total', 'sub_total'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeInGroup($query, $groupId)
    {
        return $query->where('group_id', $groupId);
    }

    public function items()
    {
        return $this->hasMany(QuotationItem::class);
    }

    public function terms()
    {
        return $this->hasMany(QuotationTerm::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
