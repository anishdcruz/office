<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationItem extends Model
{
    protected $fillable = ['unit_price', 'qty', 'description', 'ref_no'];

    public function getTotalAttribute()
    {
        return $this->attributes['unit_price'] * $this->attributes['qty'];
    }
}
