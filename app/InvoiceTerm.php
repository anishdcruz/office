<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceTerm extends Model
{
    protected $fillable = ['description'];
}
