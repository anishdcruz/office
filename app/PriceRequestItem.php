<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceRequestItem extends Model
{
    protected $fillable = ['qty', 'description', 'ref_no'];
}
