<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceRequestTerm extends Model
{
    protected $fillable = ['description'];
}
