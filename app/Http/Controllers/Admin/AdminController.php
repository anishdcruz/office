<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Admin;
use Auth;

class AdminController extends Controller
{
    public function index()
    {
        $admins = Admin::orderBy('created_at', 'desc')
            ->get();
        return view('admin.admin.index', [
            'admins' => $admins
        ]);
    }

    public function create()
    {
        return view('admin.admin.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:admins',
            'password' => 'required|confirmed|between:6,40'
        ]);

        Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        return redirect()
            ->route('admin.admin.admin.index')
            ->withMessage('Successfully created admin!');
    }

    public function destroy(Admin $admin)
    {
        if(Auth::guard('admin')->id() == $admin->id) {
            return redirect()
                ->route('admin.admin.admin.index')
                ->withError('Cannot delete yourself!');
        }

        $admin->delete();

        return redirect()
            ->route('admin.admin.admin.index')
            ->withMessage('Successfully deleted admin!');
    }
}
