<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;

class AuthController extends Controller
{
    public function show()
    {
        return view('admin.auth.show');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|between:6,40'
        ]);

        $auth = Auth::guard('admin')
            ->attempt($request->only('email', 'password'));

        if(!$auth) {
            return redirect()
                ->route('admin.auth.show')
                ->withErrors(['email' => 'Invalid email or password!']);
        }

        return redirect()
            ->route('admin.dashboard');
    }

    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect()
            ->route('admin.auth.show');
    }

    public function showSettings()
    {
        return view('admin.settings.show');
    }

    public function storeSettings(Request $request)
    {
        $this->validate($request, [
            'current_password' => 'required|between:6,40',
            'new_password' => 'required|confirmed|between:6,40'
        ]);

        if($request->current_password == $request->new_password) {
            return redirect()
                ->route('admin.settings.show')
                ->withError('New Password cannot be same as current password!');
        }

        $user = Auth::guard('admin')->user();

        if(Hash::check($request->current_password, $user->password)) {
            $user->password = bcrypt($request->new_password);
            $user->save();
            Auth::guard('admin')->logout();
            return redirect()
                ->route('admin.auth.show');
        }

        return redirect()
                ->route('admin.settings.show')
                ->withError('current password is wrong!');
    }
}
