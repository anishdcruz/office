<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Invitation;
use Mail;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('created_at', 'desc')
            ->get();
        return view('admin.user.index', [
            'users' => $users
        ]);
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'user_type' => 'required|in:1,2'
        ]);

        $invite = Invitation::where('email', $request->email)
            ->first();

        if($invite) {
            return redirect()
                ->route('admin.admin.user.index')
                ->withWarning('Invitation already send!');
        }

        //generate token
        $token = str_random(128);

        while (Invitation::whereToken($token)->first()) {
            $token = str_random(128);
        }

        // invite to join app
        $invitation = Invitation::create([
            'email' => $request->email,
            'token' => $token,
            'user_type' => $request->user_type
        ]);

        Mail::queue('emails.invite', ['invitation' => $invitation], function ($m) use ($invitation) {
            $m->from('sandbox@sparkpostbox.com', 'Your Application');

            $m->to($invitation->email)->subject('Invitation to join');
        });

        return redirect()
            ->route('admin.admin.user.index')
            ->withMessage('Successfully send Invitation!');
    }

    public function ban(User $user)
    {
        if($user->is_banned) {
            $user->is_banned = 0;
        } else {
            $user->is_banned = 1;
        }

        $user->save();

        return redirect()
            ->route('admin.admin.user.index')
            ->withMessage('Successfully banned user!');
    }
}
