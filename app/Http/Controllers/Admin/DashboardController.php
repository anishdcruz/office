<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function show()
    {
        $items = [];

        $items[] = [
            'size' => 3,
            'count' => \App\Group::count(),
            'title' => 'Group'
        ];

        $items[] = [
            'size' => 3,
            'count' => \App\User::count(),
            'title' => 'User'
        ];

        $items[] = [
            'size' => 3,
            'count' => \App\Product::count(),
            'title' => 'Product'
        ];

        $items[] = [
            'size' => 3,
            'count' => \App\Invoice::count(),
            'title' => 'Invoice'
        ];

        $items[] = [
            'size' => 3,
            'count' => \App\PurchaseOrder::count(),
            'title' => 'Purchase Order'
        ];

        $items[] = [
            'size' => 3,
            'count' => \App\PriceRequest::count(),
            'title' => 'Price Request'
        ];

        $items[] = [
            'size' => 3,
            'count' => \App\Quotation::count(),
            'title' => 'Quotation'
        ];

        $items[] = [
            'size' => 3,
            'count' => \App\Admin::count(),
            'title' => 'Admin'
        ];

        return view('admin.dashboard.show', [
            'items' => $items
        ]);
    }
}
