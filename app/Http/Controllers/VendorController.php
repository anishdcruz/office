<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Vendor;
use App\Group;
use App\Action;
use Auth;

class VendorController extends Controller
{
    public function index(Group $group)
    {
        $vendors = Vendor::inGroup($group->id)
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        return view('vendors.index', [
            'group' => $group,
            'vendors' => $vendors
        ]);
    }

    public function create(Group $group)
    {
        return view('vendors.create', [
            'group' => $group
        ]);
    }

    public function store(Request $request, Group $group)
    {
        $this->validate($request, [
            'company' => 'required|max:255',
            'person' => 'required|max:255',
            'telephone' => 'required|max:255',
            'email' => 'email',
            'address' => 'max:60000',
            'internal_note' => 'max:60000'
        ]);

        $inputs = $request->only('person', 'telephone', 'company', 'email', 'address', 'internal_note');
        $inputs = array_add($inputs, 'group_id', $group->id);
        $inputs = array_add($inputs, 'user_id', Auth::id());

        $vendor = Vendor::create($inputs);
        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'created vendor with id '.$vendor->id,
        ]);

        return redirect()
            ->route('group.{group}.vendor.index', [$group])
            ->withMessage('Successfully created vendor');
    }

    public function show(Group $group, $vendorId)
    {
        $vendor = Vendor::inGroup($group->id)
            ->findOrFail($vendorId);

        return view('vendors.show', [
            'group' => $group,
            'vendor' => $vendor
        ]);
    }

    public function edit(Group $group, $vendorId)
    {
        $vendor = Vendor::inGroup($group->id)
            ->findOrFail($vendorId);

        return view('vendors.edit', [
            'group' => $group,
            'vendor' => $vendor
        ]);
    }

    public function update(Request $request, Group $group, $vendorId)
    {
        $this->validate($request, [
            'company' => 'required|max:255',
            'person' => 'required|max:255',
            'telephone' => 'required|max:255',
            'email' => 'email',
            'address' => 'max:60000',
            'internal_note' => 'max:60000'
        ]);

        $vendor = Vendor::inGroup($group->id)
            ->findOrFail($vendorId);

        $vendor->update($request->only('person', 'telephone', 'company', 'email', 'address', 'internal_note'));

        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'updated vendor with id '.$vendor->id,
        ]);

        return redirect()
            ->route('group.{group}.vendor.index', [$group])
            ->withMessage('Successfully updated vendor');
    }

    public function destroy(Group $group, $vendorId)
    {
        $vendor = Vendor::inGroup($group->id)
            ->findOrFail($vendorId);

        if($vendor->user_id == Auth::id() || $group->owner_id == Auth::id()) {
            $id = $vendor->id;
            $vendor->delete();

            Action::create([
                'group_id' => $group->id,
                'user_id' => Auth::id(),
                'description' => 'deleted vendor with id '.$id,
            ]);

            return redirect()
                ->route('group.{group}.vendor.index', [$group])
                ->withMessage('Successfully deleted vendor');
        }

        return redirect()
            ->route('group.{group}.vendor.index', [$group])
            ->withError('Only the owner of the vendor can delete!');
    }

}
