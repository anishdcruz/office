<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Group;
use App\Action;
use App\Client;
use Auth;

class ClientController extends Controller
{
    public function index(Group $group)
    {
        $clients = Client::inGroup($group->id)
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        return view('client.index', [
            'group' => $group,
            'clients' => $clients
        ]);
    }

    public function create(Group $group)
    {
        return view('client.create', [
            'group' => $group
        ]);
    }

    public function store(Request $request, Group $group)
    {
        $this->validate($request, [
            'company' => 'required|max:255',
            'person' => 'required|max:255',
            'telephone' => 'required|max:255',
            'email' => 'email',
            'address' => 'max:60000',
            'internal_note' => 'max:60000'
        ]);

        $inputs = $request->only('person', 'telephone', 'company', 'email', 'address', 'internal_note');
        $inputs = array_add($inputs, 'group_id', $group->id);
        $inputs = array_add($inputs, 'user_id', Auth::id());

        $client = Client::create($inputs);

        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'created client with id '.$client->id,
        ]);

        return redirect()
            ->route('group.{group}.client.index', [$group])
            ->withMessage('Successfully created client');
    }

    public function show(Group $group, $clientId)
    {
        $client = Client::inGroup($group->id)
            ->findOrFail($clientId);

        return view('client.show', [
            'group' => $group,
            'client' => $client
        ]);
    }

    public function edit(Group $group, $clientId)
    {
        $client = Client::inGroup($group->id)
            ->findOrFail($clientId);

        return view('client.edit', [
            'group' => $group,
            'client' => $client
        ]);
    }

    public function update(Request $request, Group $group, $clientId)
    {
        $this->validate($request, [
            'company' => 'required|max:255',
            'person' => 'required|max:255',
            'telephone' => 'required|max:255',
            'email' => 'email',
            'address' => 'max:60000',
            'internal_note' => 'max:60000'
        ]);

        $client = Client::inGroup($group->id)
            ->findOrFail($clientId);

        $client->update($request->only('person', 'telephone', 'company', 'email', 'address', 'internal_note'));

        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'updated client with id '.$client->id,
        ]);

        return redirect()
            ->route('group.{group}.client.index', [$group])
            ->withMessage('Successfully updated client');
    }

    public function destroy(Group $group, $clientId)
    {
        $client = Client::inGroup($group->id)
            ->findOrFail($clientId);

        if($client->user_id == Auth::id() || $group->owner_id == Auth::id()) {
            $id = $client->id;
            $client->delete();

            Action::create([
                'group_id' => $group->id,
                'user_id' => Auth::id(),
                'description' => 'deleted client with id '.$client->id,
            ]);

            return redirect()
                ->route('group.{group}.client.index', [$group])
                ->withMessage('Successfully deleted client');
        }

        return redirect()
            ->route('group.{group}.client.index', [$group])
            ->withError('Only the owner of the client can delete!');
    }

}
