<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Group;
use App\Action;
use App\Client;
use App\Quotation;
use App\QuotationItem;
use App\QuotationTerm;
use Auth;

class QuotationController extends Controller
{
    public function index(Group $group)
    {
        $quotations = Quotation::inGroup($group->id)
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        return view('quotation.index', [
            'group' => $group,
            'quotations' => $quotations
        ]);
    }

    public function create(Group $group)
    {
        $clients = Client::inGroup($group->id)
            ->get(['id', 'company', 'person']);

        $quotation = new Quotation([
            'client_id' => 0,
            'quotation_no' => null,
            'title' => '',
            'quotation_date' => '',
            'internal_note' => '',
            'discount' => 0,
            'in_words' => ''
        ]);

        return view('quotation.create', [
            'group' => $group,
            'quotation' => $quotation,
            'clients' => $clients
        ]);
    }

    public function store(Request $request, Group $group)
    {
        $this->validate($request, [
            'client_id' => 'required|integer|exists:clients,id,group_id,'.$group->id,
            'quotation_no' => 'integer|unique:quotations,quotation_no,'.$group->id.',group_id',
            'title' => 'required|max:255',
            'quotation_date' => 'required|date_format:Y-m-d',
            'internal_note' => 'max:60000',
            'discount' => 'numeric|min:0',
            'in_words' => 'required|max:255',
            'products.*.ref_no' => 'required|alpha_num|max:255',
            'products.*.description' => 'required|max:60000',
            'products.*.qty' => 'required|integer|min:1',
            'products.*.unit_price' => 'required|numeric',
            'terms.*.description' => 'required:max:60000'
        ]);

        $inputs = $request->only(
            'client_id', 'quotation_no', 'title',
            'quotation_date', 'due_date', 'internal_note',
            'discount', 'in_words'
        );

        if(!$request->has('quotation_no')) {
            $inv = 1000;
            $record = Quotation::inGroup($group->id)
                ->orderBy('created_at', 'desc')
                ->first();
            if(!is_null($record)) {
                $inv = $record->quotation_no + 1;
            }
            $inputs['quotation_no'] = $inv;
        }

        $items = [];
        $terms = [];
        $subTotal = 0;

        foreach($request->terms as $term) {
            $terms[] = new QuotationTerm([
                'description' => $term['description']
            ]);
        }

        foreach($request->products as $item) {
            $items[] = new QuotationItem([
                'ref_no' => $item['ref_no'],
                'description' => $item['description'],
                'unit_price' => $item['unit_price'],
                'qty' => $item['qty']
            ]);
            $subTotal += $item['unit_price'] * $item['qty'];
        }

        $grandTotal = $subTotal - $request->discount;

        $inputs = array_add($inputs, 'sub_total', $subTotal);
        $inputs = array_add($inputs, 'grand_total', $grandTotal);
        $inputs = array_add($inputs, 'group_id', $group->id);
        $inputs = array_add($inputs, 'user_id', Auth::id());

        $quotation = Quotation::create($inputs);

        $quotation->items()
            ->saveMany($items);

        $quotation->terms()
            ->saveMany($terms);

        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'created quotation with id '.$quotation->id,
        ]);

        return response()
            ->json([
                'created' => true,
                'id' => $quotation->id
            ]);
    }

    public function show(Group $group, $quotationId)
    {
        $quotation = Quotation::inGroup($group->id)
            ->findOrFail($quotationId);
        return view('quotation.show', [
            'group' => $group,
            'quotation' => $quotation
        ]);
    }

    public function edit(Group $group, $quotationId)
    {
        $quotation = Quotation::inGroup($group->id)
            ->findOrFail($quotationId);
        $clients = Client::inGroup($group->id)
            ->get(['id', 'company', 'person']);
        return view('quotation.edit', [
            'group' => $group,
            'quotation' => $quotation,
            'clients' => $clients
        ]);
    }

    public function update(Request $request, Group $group, $quotationId)
    {
        $this->validate($request, [
            'client_id' => 'required|integer|exists:clients,id,group_id,'.$group->id,
            'quotation_no' => 'integer|unique:quotations,quotation_no,'.$group->id.',group_id',
            'title' => 'required|max:255',
            'quotation_date' => 'required|date_format:Y-m-d',
            'internal_note' => 'max:60000',
            'discount' => 'numeric|min:0',
            'in_words' => 'required|max:255',
            'products.*.ref_no' => 'required|alpha_num|max:255',
            'products.*.description' => 'required|max:60000',
            'products.*.qty' => 'required|integer|min:1',
            'products.*.unit_price' => 'required|numeric',
            'terms.*.description' => 'required:max:60000'
        ]);

        $quotation = Quotation::inGroup($group->id)
            ->findOrFail($quotationId);

        $inputs = $request->only(
            'client_id', 'quotation_no', 'title',
            'quotation_date', 'due_date', 'internal_note',
            'discount', 'in_words'
        );

        if($request->has('quotation_no')) {
            $inputs['quotation_no'] = $request->get('quotation_no');
        }

        $items = [];
        $terms = [];
        $subTotal = 0;

        foreach($request->terms as $term) {
            $terms[] = new QuotationTerm([
                'description' => $term['description']
            ]);
        }

        foreach($request->products as $item) {
            $items[] = new QuotationItem([
                'ref_no' => $item['ref_no'],
                'description' => $item['description'],
                'unit_price' => $item['unit_price'],
                'qty' => $item['qty']
            ]);
            $subTotal += $item['unit_price'] * $item['qty'];
        }

        $grandTotal = $subTotal - $request->discount;

        $inputs = array_add($inputs, 'sub_total', $subTotal);
        $inputs = array_add($inputs, 'grand_total', $grandTotal);

        $quotation->update($inputs);

        $quotation->items()
            ->delete();

        $quotation->items()
            ->saveMany($items);

        $quotation->terms()
            ->delete();

        $quotation->terms()
            ->saveMany($terms);

        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'updated quotation with id '.$quotation->id,
        ]);

        return response()
            ->json([
                'updated' => true,
                'id' => $quotation->id
            ]);
    }

    public function destroy(Group $group, $quotationId)
    {
        $quotation = Quotation::inGroup($group->id)
            ->findOrFail($quotationId);

        if($quotation->user_id == Auth::id() || $group->owner_id == Auth::id()) {
            $id = $quotation->id;
            $quotation->items()
                ->delete();

            $quotation->terms()
                ->delete();

            $quotation->delete();

            Action::create([
                'group_id' => $group->id,
                'user_id' => Auth::id(),
                'description' => 'deleted quotation with id '.$id,
            ]);

            return redirect()
                ->route('group.{group}.quotation.index', [$group])
                ->withMessage('Successfully deleted quotation');
        }

        return redirect()
            ->route('group.{group}.quotation.index', [$group])
            ->withError('Only the owner of the quotation can delete!');
    }
}
