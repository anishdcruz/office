<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;

class BasicController extends Controller
{
    public function index()
    {
        return view('basic.index');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required|between:6,255'
        ]);

        $auth = Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
            'is_banned' => 0
        ], $request->has('remember_me'));

        if(!$auth) {
            return redirect()
                ->to('/')
                ->withInput()
                ->withErrors([
                    'email' => 'Invalid username or password!'
                ]);
        }

        return redirect()->intended('group');
    }

    public function logout()
    {
        Auth::logout();

        return redirect()
            ->to('/');
    }
}
