<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\PriceRequest;
use App\PriceRequestItem;
use App\PriceRequestTerm;
use App\Group;
use App\Vendor;
use App\Action;
use Auth;

class PriceRequestController extends Controller
{
    public function index(Group $group)
    {
        $priceRequests = PriceRequest::inGroup($group->id)
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('price_request.index', [
            'group' => $group,
            'priceRequests' => $priceRequests
        ]);
    }

    public function create(Group $group)
    {
        $vendors = Vendor::inGroup($group->id)
            ->get(['id', 'company', 'person']);

        $priceRequest = new PriceRequest([
            'vendor_id' => 0,
            'price_request_no' => null,
            'title' => '',
            'price_request_date' => '',
            'internal_note' => '',
        ]);

        return view('price_request.create', [
            'group' => $group,
            'priceRequest' => $priceRequest,
            'vendors' => $vendors
        ]);
    }

    public function store(Request $request, Group $group)
    {
        $this->validate($request, [
            'vendor_id' => 'required|integer|exists:vendors,id,group_id,'.$group->id,
            'price_request_no' => 'integer|unique:price_requests,price_request_no,'.$group->id.',group_id',
            'title' => 'required|max:255',
            'price_request_date' => 'required|date_format:Y-m-d',
            'internal_note' => 'max:60000',
            'products.*.ref_no' => 'required|alpha_num|max:255',
            'products.*.description' => 'required|max:60000',
            'products.*.qty' => 'required|integer|min:1',
            'terms.*.description' => 'required:max:60000'
        ]);

        $inputs = $request->only(
            'vendor_id', 'price_request_no', 'title',
            'invoice_date', 'internal_note', 'price_request_date'
        );

        if(!$request->has('price_request_no')) {
            $inv = 1000;
            $record = PriceRequest::inGroup($group->id)
                ->orderBy('created_at', 'desc')
                ->first();
            if(!is_null($record)) {
                $inv = $record->price_request_no + 1;
            }
            $inputs['price_request_no'] = $inv;
        }

        $items = [];
        $terms = [];

        foreach($request->terms as $term) {
            $terms[] = new PriceRequestTerm([
                'description' => $term['description']
            ]);
        }

        foreach($request->products as $item) {
            $items[] = new PriceRequestItem([
                'ref_no' => $item['ref_no'],
                'description' => $item['description'],
                'qty' => $item['qty']
            ]);
        }

        $inputs = array_add($inputs, 'group_id', $group->id);
        $inputs = array_add($inputs, 'user_id', Auth::id());

        $priceRequest = PriceRequest::create($inputs);

        $priceRequest->items()
            ->saveMany($items);

        $priceRequest->terms()
            ->saveMany($terms);

        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'created Price Request with id '.$priceRequest->id,
        ]);

        return response()
            ->json([
                'created' => true,
                'id' => $priceRequest->id
            ]);
    }

    public function show(Group $group, $priceRequestId)
    {
        $priceRequest = PriceRequest::inGroup($group->id)
            ->findOrFail($priceRequestId);
        return view('price_request.show', [
            'group' => $group,
            'priceRequest' => $priceRequest,
        ]);
    }

    public function edit(Group $group, $priceRequestId)
    {
        $priceRequest = PriceRequest::inGroup($group->id)
            ->findOrFail($priceRequestId);
        $vendors = Vendor::inGroup($group->id)
            ->get(['id', 'company', 'person']);
        return view('price_request.edit', [
            'group' => $group,
            'priceRequest' => $priceRequest,
            'vendors' => $vendors
        ]);
    }

    public function update(Request $request, Group $group, $priceRequestId)
    {
        $this->validate($request, [
            'vendor_id' => 'required|integer|exists:vendors,id,group_id,'.$group->id,
            'price_request_no' => 'integer|unique:price_requests,price_request_no,'.$group->id.',group_id',
            'title' => 'required|max:255',
            'price_request_date' => 'required|date_format:Y-m-d',
            'internal_note' => 'max:60000',
            'products.*.ref_no' => 'required|alpha_num|max:255',
            'products.*.description' => 'required|max:60000',
            'products.*.qty' => 'required|integer|min:1',
            'terms.*.description' => 'required:max:60000'
        ]);

        $priceRequest = PriceRequest::inGroup($group->id)
            ->findOrFail($priceRequestId);

        $inputs = $request->only(
            'vendor_id', 'price_request_no', 'title',
            'invoice_date', 'internal_note', 'price_request_date'
        );

        if($request->has('price_request_no')) {
            $inputs['price_request_no'] = $request->get('price_request_no');
        }

        $items = [];
        $terms = [];

        foreach($request->terms as $term) {
            $terms[] = new PriceRequestTerm([
                'description' => $term['description']
            ]);
        }

        foreach($request->products as $item) {
            $items[] = new PriceRequestItem([
                'ref_no' => $item['ref_no'],
                'description' => $item['description'],
                'qty' => $item['qty']
            ]);
        }

        $priceRequest->update($inputs);

        $priceRequest->items()
            ->delete();
        $priceRequest->items()
            ->saveMany($items);

        $priceRequest->terms()
            ->delete();
        $priceRequest->terms()
            ->saveMany($terms);

        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'updated Price Request with id '.$priceRequest->id,
        ]);

        return response()
            ->json([
                'updated' => true,
                'id' => $priceRequest->id
            ]);
    }

    public function destroy(Group $group, $priceRequestId)
    {
        $priceRequest = PriceRequest::inGroup($group->id)
            ->findOrFail($priceRequestId);

        if($priceRequest->user_id == Auth::id() || $group->owner_id == Auth::id()) {
            $id = $priceRequest->id;
            $priceRequest->items()
                ->delete();

            $priceRequest->terms()
                ->delete();

            $priceRequest->delete();

            Action::create([
                'group_id' => $group->id,
                'user_id' => Auth::id(),
                'description' => 'deleted price Request with id '.$id,
            ]);

            return redirect()
                ->route('group.{group}.price-request.index', [$group])
                ->withMessage('Successfully deleted price Request');
        }

        return redirect()
            ->route('group.{group}.price-request.index', [$group])
            ->withError('Only the owner of the price Request can delete!');
    }
}
