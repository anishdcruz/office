<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Group;
use App\Product;
use App\Action;
use Auth;

class ProductController extends Controller
{
    public function index(Group $group)
    {
        $products = Product::inGroup($group->id)
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        return view('product.index', [
            'group' => $group,
            'products' => $products
        ]);
    }

    public function create(Group $group)
    {
        return view('product.create', [
            'group' => $group
        ]);
    }

    public function store(Request $request, Group $group)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'ref_no' => 'required|max:255',
            'unit_price' => 'required|numeric',
            'description' => 'required|max:60000'
        ]);

        $inputs = $request->only('name', 'ref_no', 'unit_price', 'description');
        $inputs = array_add($inputs, 'group_id', $group->id);
        $inputs = array_add($inputs, 'user_id', Auth::id());

        $product = Product::create($inputs);
        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'created product with id '.$product->id,
        ]);

        return redirect()
            ->route('group.{group}.product.index', [$group])
            ->withMessage('Successfully created Product');
    }

    public function show(Group $group, $productId)
    {
        $product = Product::inGroup($group->id)
            ->findOrFail($productId);

        return view('product.show', [
            'group' => $group,
            'product' => $product
        ]);
    }

    public function edit(Group $group, $productId)
    {
        $product = Product::inGroup($group->id)
            ->findOrFail($productId);

        return view('product.edit', [
            'group' => $group,
            'product' => $product
        ]);
    }

    public function update(Request $request, Group $group, $productId)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'ref_no' => 'required|max:255',
            'unit_price' => 'required|numeric',
            'description' => 'required|max:60000'
        ]);

        $product = Product::inGroup($group->id)
            ->findOrFail($productId);

        $inputs = $request->only('name', 'ref_no', 'unit_price', 'description');
        $product->update($inputs);

        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'updated product with id '.$product->id,
        ]);

        return redirect()
            ->route('group.{group}.product.index', [$group])
            ->withMessage('Successfully updated Product');
    }

    public function destroy(Group $group, $productId)
    {
        $product = Product::inGroup($group->id)
            ->findOrFail($productId);

        if($product->user_id == Auth::id() || $group->owner_id == Auth::id()) {
            $id = $product->id;
            $product->delete();

            Action::create([
                'group_id' => $group->id,
                'user_id' => Auth::id(),
                'description' => 'deleted terms with id '.$id,
            ]);

            return redirect()
                ->route('group.{group}.product.index', [$group])
                ->withMessage('Successfully deleted Product');
        }

        return redirect()
            ->route('group.{group}.product.index', [$group])
            ->withError('Only the owner of the product can delete!');
    }

}
