<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Group;
use App\Product;
use App\Terms;

class ApiController extends Controller
{
    public function showProducts(Request $request, Group $group)
    {
        $q = $request->get('query', null);
        $response = [];

        if(!is_null($q)) {
            $response = Product::inGroup($group->id)
                ->where('name', 'like', '%'.$q.'%')
                ->limit(4)
                ->get(['ref_no', 'name', 'description', 'unit_price']);
        }

        return response()
            ->json([
                'products' => $response
            ]);
    }

    public function showTerms(Request $request, Group $group)
    {
        $q = $request->get('query', null);
        $response = [];

        if(!is_null($q)) {
            $response = Terms::inGroup($group->id)
                ->where('name', 'like', '%'.$q.'%')
                ->limit(4)
                ->get(['id', 'name', 'description']);
        }

        return response()
            ->json([
                'terms' => $response
            ]);
    }
}
