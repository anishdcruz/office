<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Group;
use App\Vendor;
use App\Action;
use App\PurchaseOrder;
use App\PurchaseOrderTerm;
use App\PurchaseOrderItem;
use Auth;

class PurchaseOrderController extends Controller
{
    public function index(Group $group)
    {
        $purchaseOrders = PurchaseOrder::inGroup($group->id)
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('purchase_order.index', [
            'group' => $group,
            'purchaseOrders' => $purchaseOrders
        ]);
    }

    public function create(Group $group)
    {
        $vendors = Vendor::inGroup($group->id)
            ->get(['id', 'company', 'person']);

        $purchaseOrder = new PurchaseOrder([
            'vendor_id' => 0,
            'purchase_order_no' => null,
            'title' => '',
            'purchase_order_date' => null,
            'internal_note' => '',
            'in_words' => ''
        ]);

        return view('purchase_order.create', [
            'group' => $group,
            'purchaseOrder' => $purchaseOrder,
            'vendors' => $vendors
        ]);
    }

    public function store(Request $request, Group $group)
    {
        $this->validate($request, [
            'vendor_id' => 'required|integer|exists:clients,id,group_id,'.$group->id,
            'purchase_order_no' => 'integer|unique:purchase_orders,purchase_order_no,'.$group->id.',group_id',
            'title' => 'required|max:255',
            'purchase_order_date' => 'required|date_format:Y-m-d',
            'internal_note' => 'max:60000',
            'in_words' => 'required|max:255',
            'products.*.ref_no' => 'required|alpha_num|max:255',
            'products.*.description' => 'required|max:60000',
            'products.*.qty' => 'required|integer|min:1',
            'products.*.unit_price' => 'required|numeric',
            'terms.*.description' => 'required:max:60000'
        ]);

        $inputs = $request->only(
            'vendor_id', 'purchase_order_no', 'title',
            'purchase_order_date', 'internal_note',
            'in_words'
        );

        if(!$request->has('purchase_order_no')) {
            $inv = 1000;
            $record = PurchaseOrder::inGroup($group->id)
                ->orderBy('created_at', 'desc')
                ->first();
            if(!is_null($record)) {
                $inv = $record->purchase_order_no + 1;
            }
            $inputs['purchase_order_no'] = $inv;
        }

        $items = [];
        $terms = [];
        $grandTotal = 0;

        foreach($request->terms as $term) {
            $terms[] = new PurchaseOrderTerm([
                'description' => $term['description']
            ]);
        }

        foreach($request->products as $item) {
            $items[] = new PurchaseOrderItem([
                'ref_no' => $item['ref_no'],
                'description' => $item['description'],
                'unit_price' => $item['unit_price'],
                'qty' => $item['qty']
            ]);
            $grandTotal += $item['unit_price'] * $item['qty'];
        }

        $inputs = array_add($inputs, 'grand_total', $grandTotal);
        $inputs = array_add($inputs, 'group_id', $group->id);
        $inputs = array_add($inputs, 'user_id', Auth::id());

        $purchaseOrder = PurchaseOrder::create($inputs);

        $purchaseOrder->items()
            ->saveMany($items);

        $purchaseOrder->terms()
            ->saveMany($terms);

        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'created purchase order with id '.$purchaseOrder->id,
        ]);

        return response()
            ->json([
                'created' => true,
                'id' => $purchaseOrder->id
            ]);
    }

    public function show(Group $group, $purchaseOrderId)
    {
        $purchaseOrder = PurchaseOrder::inGroup($group->id)
            ->findOrFail($purchaseOrderId);
        return view('purchase_order.show', [
            'group' => $group,
            'purchaseOrder' => $purchaseOrder
        ]);
    }

    public function edit(Group $group, $purchaseOrderId)
    {
        $purchaseOrder = PurchaseOrder::inGroup($group->id)
            ->findOrFail($purchaseOrderId);

        $vendors = Vendor::inGroup($group->id)
            ->get(['id', 'company', 'person']);

        return view('purchase_order.edit', [
            'group' => $group,
            'purchaseOrder' => $purchaseOrder,
            'vendors' => $vendors
        ]);
    }

    public function update(Request $request, Group $group, $purchaseOrderId)
    {
        $this->validate($request, [
            'vendor_id' => 'required|integer|exists:clients,id,group_id,'.$group->id,
            'purchase_order_no' => 'integer|unique:purchase_orders,purchase_order_no,'.$group->id.',group_id',
            'title' => 'required|max:255',
            'purchase_order_date' => 'required|date_format:Y-m-d',
            'internal_note' => 'max:60000',
            'in_words' => 'required|max:255',
            'products.*.ref_no' => 'required|alpha_num|max:255',
            'products.*.description' => 'required|max:60000',
            'products.*.qty' => 'required|integer|min:1',
            'products.*.unit_price' => 'required|numeric',
            'terms.*.description' => 'required:max:60000'
        ]);

        $purchaseOrder = PurchaseOrder::inGroup($group->id)
            ->findOrFail($purchaseOrderId);

        $inputs = $request->only(
            'vendor_id', 'purchase_order_no', 'title',
            'purchase_order_date', 'internal_note',
            'in_words'
        );

        $items = [];
        $terms = [];
        $grandTotal = 0;

        foreach($request->terms as $term) {
            $terms[] = new PurchaseOrderTerm([
                'description' => $term['description']
            ]);
        }

        foreach($request->products as $item) {
            $items[] = new PurchaseOrderItem([
                'ref_no' => $item['ref_no'],
                'description' => $item['description'],
                'unit_price' => $item['unit_price'],
                'qty' => $item['qty']
            ]);
            $grandTotal += $item['unit_price'] * $item['qty'];
        }

        $inputs = array_add($inputs, 'grand_total', $grandTotal);

        $purchaseOrder->update($inputs);

        $purchaseOrder->items()
            ->delete();
        $purchaseOrder->items()
            ->saveMany($items);

        $purchaseOrder->terms()
            ->delete();
        $purchaseOrder->terms()
            ->saveMany($terms);

        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'updated purchase order with id '.$purchaseOrder->id,
        ]);

        return response()
            ->json([
                'updated' => true,
                'id' => $purchaseOrder->id
            ]);
    }

    public function destroy(Group $group, $purchaseOrderId)
    {
        $purchaseOrder = PurchaseOrder::inGroup($group->id)
            ->findOrFail($purchaseOrderId);

        if($purchaseOrder->user_id == Auth::id() || $group->owner_id == Auth::id()) {
            $id = $purchaseOrder->id;
            $purchaseOrder->items()
                ->delete();

            $purchaseOrder->terms()
                ->delete();

            $purchaseOrder->delete();

            Action::create([
                'group_id' => $group->id,
                'user_id' => Auth::id(),
                'description' => 'deleted purchase order with id '.$id,
            ]);

            return redirect()
                ->route('group.{group}.purchase-order.index', [$group])
                ->withMessage('Successfully deleted purchase order');
        }

        return redirect()
            ->route('group.{group}.purchase-order.index', [$group])
            ->withError('Only the owner of the purchase order can delete!');
    }

}
