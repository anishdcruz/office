<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Terms;
use App\Group;
use App\Action;
use Auth;

class TermsController extends Controller
{
    public function index(Group $group)
    {
        $terms = Terms::inGroup($group->id)
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        return view('terms.index', [
            'group' => $group,
            'termss' => $terms
        ]);
    }

    public function create(Group $group)
    {
        return view('terms.create', [
            'group' => $group
        ]);
    }

    public function store(Request $request, Group $group)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'max:60000'
        ]);

        $inputs = $request->only('name', 'description');
        $inputs = array_add($inputs, 'group_id', $group->id);
        $inputs = array_add($inputs, 'user_id', Auth::id());

        $terms = Terms::create($inputs);

        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'created terms with id '.$terms->id,
        ]);

        return redirect()
            ->route('group.{group}.terms.index', [$group])
            ->withMessage('Successfully created terms');
    }

    public function show(Group $group, $termsId)
    {
        $terms = Terms::inGroup($group->id)
            ->findOrFail($termsId);

        return view('terms.show', [
            'group' => $group,
            'terms' => $terms
        ]);
    }

    public function edit(Group $group, $termsId)
    {
        $terms = Terms::inGroup($group->id)
            ->findOrFail($termsId);

        return view('terms.edit', [
            'group' => $group,
            'terms' => $terms
        ]);
    }

    public function update(Request $request, Group $group, $termsId)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'max:60000'
        ]);

        $terms = Terms::inGroup($group->id)
            ->findOrFail($termsId);

        $terms->update($request->only('name', 'description'));

        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'updated terms with id '.$terms->id,
        ]);
        return redirect()
            ->route('group.{group}.terms.index', [$group])
            ->withMessage('Successfully updated terms');
    }

    public function destroy(Group $group, $termsId)
    {
        $terms = Terms::inGroup($group->id)
            ->findOrFail($termsId);

        if($terms->user_id == Auth::id() || $group->owner_id == Auth::id()) {
            $id = $terms->id;
            $terms->delete();

            Action::create([
                'group_id' => $group->id,
                'user_id' => Auth::id(),
                'description' => 'deleted terms with id '.$id,
            ]);

            return redirect()
                ->route('group.{group}.terms.index', [$group])
                ->withMessage('Successfully deleted terms');
        }

        return redirect()
            ->route('group.{group}.terms.index', [$group])
            ->withError('Only the owner of the terms can delete!');
    }

}
