<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\GroupMember;
use App\Group;
use App\Action;
use Auth;

class GroupController extends Controller
{
    public function index()
    {
        $groups = GroupMember::with('group')
            ->where('user_id', Auth::id())
            ->orderBy('created_at', 'desc')
            ->get();

        return view('group.index', [
            'groups' => $groups
        ]);
    }

    public function create()
    {
        return view('group.create');
    }

    public function show($id)
    {
        $group = Group::whereHas('members', function($query) {
            $query->where('user_id', Auth::id());
        })
        ->where('id', $id)
        ->first();

        $actions = Action::where('group_id', $group->id)
            ->orderBy('created_at', 'desc')
            ->limit(20)
            ->get();

        return view('group.show', [
            'group' => $group,
            'actions' => $actions
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255'
        ]);

        $group = Group::create([
            'owner_id' => Auth::id(),
            'name' => $request->name
        ]);

        GroupMember::create([
            'user_id' => Auth::id(),
            'group_id' => $group->id
        ]);

        return redirect()
            ->route('group.index')
            ->withMessage('Successfully created group!');
    }
}
