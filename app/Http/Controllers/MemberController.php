<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Group;
use App\GroupMember;
use App\Action;
use Auth;
use App\Invitation;
use App\User;
use Mail;

class MemberController extends Controller
{
    public function index(Group $group)
    {
        return view('member.index', [
            'group' => $group
        ]);
    }

    public function create(Group $group)
    {
        if($group->owner_id != Auth::id()) {
            return abort(404);
        }

        return view('member.create', [
            'group' => $group
        ]);

    }

    public function store(Request $request, Group $group)
    {
        $this->validate($request, [
            'email' => 'required|email|exists:users'
        ]);

        $user = User::where('email', $request->email)
            ->first();

        // check if already a member
        $member = GroupMember::inGroup($group->id)
            ->where('user_id', $user->id)
            ->first();

        if($member) {
            return redirect()
                ->route('group.{group}.member.index', [$group])
                ->withWarning('the user is already a member');
        }

        GroupMember::create([
            'user_id' => $user->id,
            'group_id' => $group->id
        ]);

        Mail::send('emails.existing', ['group' => $group], function ($m) use ($user, $group) {
            $m->from('sandbox@sparkpostbox.com', 'Your Application');

            $m->to($user->email)->subject('Invitation to join '. $group->name);
        });

        return redirect()
            ->route('group.{group}.member.index', [$group])
            ->withMessage('Successfully send Invitation!');
    }

    public function destroy(Group $group, $memberId)
    {
        $member = GroupMember::inGroup($group->id)
            ->findOrFail($memberId);

        if($group->owner_id == Auth::id()) {
            $id = $member->id;
            $member->delete();

            Action::create([
                'group_id' => $group->id,
                'user_id' => Auth::id(),
                'description' => 'deleted member with id '.$id,
            ]);

            return redirect()
                ->route('group.{group}.member.index', [$group])
                ->withMessage('Successfully deleted member');
        }

        return redirect()
            ->route('group.{group}.member.index', [$group])
            ->withError('Only the owner of the group can delete!');

    }
}
