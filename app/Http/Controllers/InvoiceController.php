<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Group;
use App\Client;
use App\InvoiceItem;
use App\InvoiceTerm;
use App\Invoice;
use App\Action;
use Auth;

class InvoiceController extends Controller
{
    public function index(Group $group)
    {
        $invoices = Invoice::inGroup($group->id)
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        return view('invoice.index', [
            'group' => $group,
            'invoices' => $invoices
        ]);
    }

    public function create(Group $group)
    {
        $clients = Client::inGroup($group->id)
            ->get(['id', 'company', 'person']);

        $invoice = new Invoice([
            'client_id' => 0,
            'invoice_no' => null,
            'title' => '',
            'invoice_date' => '',
            'due_date' => '',
            'internal_note' => '',
            'discount' => 0,
            'in_words' => ''
        ]);

        return view('invoice.create', [
            'group' => $group,
            'invoice' => $invoice,
            'clients' => $clients
        ]);
    }

    public function store(Request $request, Group $group)
    {
        $this->validate($request, [
            'client_id' => 'required|integer|exists:clients,id,group_id,'.$group->id,
            'invoice_no' => 'integer|unique:invoices,invoice_no,'.$group->id.',group_id',
            'title' => 'required|max:255',
            'invoice_date' => 'required|date_format:Y-m-d',
            'due_date' => 'required|date_format:Y-m-d',
            'internal_note' => 'max:60000',
            'discount' => 'numeric|min:0',
            'in_words' => 'required|max:255',
            'products.*.ref_no' => 'required|alpha_num|max:255',
            'products.*.description' => 'required|max:60000',
            'products.*.qty' => 'required|integer|min:1',
            'products.*.unit_price' => 'required|numeric',
            'terms.*.description' => 'required:max:60000'
        ]);

        $inputs = $request->only(
            'client_id', 'invoice_no', 'title',
            'invoice_date', 'due_date', 'internal_note',
            'discount', 'in_words'
        );

        if(!$request->has('invoice_no')) {
            $inv = 1000;
            $record = Invoice::inGroup($group->id)
                ->orderBy('created_at', 'desc')
                ->first();
            if(!is_null($record)) {
                $inv = $record->invoice_no + 1;
            }
            $inputs['invoice_no'] = $inv;
        }

        $items = [];
        $terms = [];
        $subTotal = 0;

        foreach($request->terms as $term) {
            $terms[] = new InvoiceTerm([
                'description' => $term['description']
            ]);
        }

        foreach($request->products as $item) {
            $items[] = new InvoiceItem([
                'ref_no' => $item['ref_no'],
                'description' => $item['description'],
                'unit_price' => $item['unit_price'],
                'qty' => $item['qty']
            ]);
            $subTotal += $item['unit_price'] * $item['qty'];
        }

        $grandTotal = $subTotal - $request->discount;

        $inputs = array_add($inputs, 'sub_total', $subTotal);
        $inputs = array_add($inputs, 'grand_total', $grandTotal);
        $inputs = array_add($inputs, 'group_id', $group->id);
        $inputs = array_add($inputs, 'user_id', Auth::id());

        $invoice = Invoice::create($inputs);

        $invoice->items()
            ->saveMany($items);

        $invoice->terms()
            ->saveMany($terms);

        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'created invoice with id '.$invoice->id,
        ]);

        return response()
            ->json([
                'created' => true,
                'id' => $invoice->id
            ]);
    }

    public function show(Group $group, $invoiceId)
    {
        $invoice = Invoice::inGroup($group->id)
            ->findOrFail($invoiceId);
        return view('invoice.show', [
            'group' => $group,
            'invoice' => $invoice
        ]);
    }

    public function edit(Group $group, $invoiceId)
    {
        $invoice = Invoice::inGroup($group->id)
            ->findOrFail($invoiceId);
        $clients = Client::inGroup($group->id)
            ->get(['id', 'company', 'person']);
        return view('invoice.edit', [
            'group' => $group,
            'invoice' => $invoice,
            'clients' => $clients
        ]);
    }

    public function update(Request $request, Group $group, $invoiceId)
    {
        $this->validate($request, [
            'client_id' => 'required|integer|exists:clients,id,group_id,'.$group->id,
            'invoice_no' => 'integer|unique:invoices,invoice_no,'.$group->id.',group_id',
            'title' => 'required|max:255',
            'invoice_date' => 'required|date_format:Y-m-d',
            'due_date' => 'required|date_format:Y-m-d',
            'internal_note' => 'max:60000',
            'discount' => 'numeric|min:0',
            'in_words' => 'required|max:255',
            'products.*.ref_no' => 'required|alpha_num|max:255',
            'products.*.description' => 'required|max:60000',
            'products.*.qty' => 'required|integer|min:1',
            'products.*.unit_price' => 'required|numeric',
            'terms.*.description' => 'required:max:60000'
        ]);

        $invoice = Invoice::inGroup($group->id)
            ->findOrFail($invoiceId);

        $inputs = $request->only(
            'client_id', 'invoice_no', 'title',
            'invoice_date', 'due_date', 'internal_note',
            'discount', 'in_words'
        );

        if($request->has('invoice_no')) {
            $inputs['invoice_no'] = $request->get('invoice_no');
        }

        $items = [];
        $terms = [];
        $subTotal = 0;

        foreach($request->terms as $term) {
            $terms[] = new InvoiceTerm([
                'description' => $term['description']
            ]);
        }

        foreach($request->products as $item) {
            $items[] = new InvoiceItem([
                'ref_no' => $item['ref_no'],
                'description' => $item['description'],
                'unit_price' => $item['unit_price'],
                'qty' => $item['qty']
            ]);
            $subTotal += $item['unit_price'] * $item['qty'];
        }

        $grandTotal = $subTotal - $request->discount;

        $inputs = array_add($inputs, 'sub_total', $subTotal);
        $inputs = array_add($inputs, 'grand_total', $grandTotal);

        $invoice->update($inputs);

        $invoice->items()
            ->delete();

        $invoice->items()
            ->saveMany($items);

        $invoice->terms()
            ->delete();

        $invoice->terms()
            ->saveMany($terms);

        Action::create([
            'group_id' => $group->id,
            'user_id' => Auth::id(),
            'description' => 'updated invoice with id '.$invoice->id,
        ]);

        return response()
            ->json([
                'updated' => true,
                'id' => $invoice->id
            ]);
    }

    public function destroy(Group $group, $invoiceId)
    {
        $invoice = Invoice::inGroup($group->id)
            ->findOrFail($invoiceId);

        if($invoice->user_id == Auth::id() || $group->owner_id == Auth::id()) {
            $id = $invoice->id;
            $invoice->items()
                ->delete();

            $invoice->terms()
                ->delete();

            $invoice->delete();

            Action::create([
                'group_id' => $group->id,
                'user_id' => Auth::id(),
                'description' => 'deleted invoice with id '.$id,
            ]);

            return redirect()
                ->route('group.{group}.invoice.index', [$group])
                ->withMessage('Successfully deleted invoice');
        }

        return redirect()
            ->route('group.{group}.invoice.index', [$group])
            ->withError('Only the owner of the invoice can delete!');
    }

    public function showClone(Group $group, $invoiceId)
    {
        $invoice = Invoice::inGroup($group->id)
            ->findOrFail($invoiceId);
        $clients = Client::inGroup($group->id)
            ->get(['id', 'company', 'person']);
        return view('invoice.clone', [
            'group' => $group,
            'invoice' => $invoice,
            'clients' => $clients
        ]);
    }
}
