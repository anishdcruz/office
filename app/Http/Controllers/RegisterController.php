<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use App\Invitation;
use App\User;
use App\GroupMember;
use Auth;

class RegisterController extends Controller
{
    public function show(Request $request)
    {
        $v = Validator::make([
            'invitation' => $request->get('invitation'),
        ], [
            'invitation' => 'required|alpha_num'
        ]);

        if($v->fails()) {
            return abort(404);
        }

        $invite = Invitation::where('token', $request->invitation)
            ->first();

        if(!$invite) {
            return abort(404);
        }

        return view('basic.register', [
            'invite' => $invite
        ]);
    }

    public function attempt(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|confirmed|between:6,40',
            'invitation' => 'required|alpha_num'
        ]);

        $invite = Invitation::where('token', $request->invitation)
            ->where('email', $request->email)
            ->first();

        if(!$invite) {
            return abort(404);
        }

        // check if user exist
        $userExist = User::where('email', $request->email)
            ->first();

        if($userExist) {
            return abort(404);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'type' => $invite->user_type
        ]);

        $invite->delete();

        Auth::login($user);

        return redirect()
            ->to('group')
            ->withMessage('Welcome!');
    }
}
