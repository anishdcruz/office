<?php

namespace App\Http\Middleware;

use Closure;
use App\GroupMember;
use Auth;

class GroupMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->group) {
            $isMember = $request->group->members()
                ->where('user_id', Auth::id())
                ->first();

            if($isMember) {
                return $next($request);
            }
        }

        return abort(404, 'Unknown Group!');
    }
}
