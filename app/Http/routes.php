<?php

// Auth::loginUsingId(3);

Route::group(['middleware' => 'guest'], function() {
    Route::get('/', 'BasicController@index');
    Route::post('login', 'BasicController@login');
    Route::get('register', 'RegisterController@show');
    Route::post('register', 'RegisterController@attempt');
});

Route::group(['middleware' => 'auth'], function() {
    Route::get('logout', 'BasicController@logout');
    Route::resource('group', 'GroupController');

    Route::group(['prefix' => 'group/{group}', 'middleware' => 'group'], function() {
        Route::resource('invoice', 'InvoiceController');
        Route::get('invoice/{invoice}/clone', 'InvoiceController@showClone')
            ->name('group.{group}.invoice.clone');

        Route::resource('purchase-order', 'PurchaseOrderController');
        Route::resource('price-request', 'PriceRequestController');
        Route::resource('quotation', 'QuotationController');

        Route::resource('product', 'ProductController');
        Route::resource('client', 'ClientController');
        Route::resource('vendor', 'VendorController');
        Route::resource('terms', 'TermsController');

        Route::resource('member', 'MemberController');
        Route::delete('member/{member}/cancel', 'MemberController@cancel')
            ->name('group.{group}.member.cancel');
    });

    Route::group(['prefix' => 'api/{group}', 'middleware' => 'group'], function() {
        Route::get('products', 'ApiController@showProducts');
        Route::get('terms', 'ApiController@showTerms');
    });
});


Route::group(['prefix' => env('ADMIN_PREFIX'), 'namespace' => 'Admin', 'as' => 'admin.'], function() {

    Route::group(['middleware' => 'guest:admin'], function() {
        Route::get('/', 'AuthController@show')->name('auth.show');
        Route::post('login', 'AuthController@login')->name('auth.login');
    });

    Route::group(['middleware' => 'auth:admin'], function() {
        Route::get('logout', 'AuthController@logout')->name('auth.logout');
        Route::get('dashboard', 'DashboardController@show')->name('dashboard');
        Route::resource('user', 'UserController');
        Route::delete('user/{user}/ban', 'UserController@ban')->name('admin.user.ban');
        Route::resource('admin', 'AdminController');
        Route::get('settings', 'AuthController@showSettings')->name('settings.show');
        Route::post('settings', 'AuthController@storeSettings')->name('settings.store');
    });
});
