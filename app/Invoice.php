<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;

class Invoice extends Model
{
    protected $fillable = [
        'client_id', 'invoice_no', 'title',
        'invoice_date', 'due_date', 'internal_note',
        'discount', 'in_words', 'group_id', 'user_id',
        'sub_total', 'grand_total'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeInGroup($query, $groupId)
    {
        return $query->where('group_id', $groupId);
    }

    public function items()
    {
        return $this->hasMany(InvoiceItem::class);
    }

    public function terms()
    {
        return $this->hasMany(InvoiceTerm::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
