<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Admin;

class InstallUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create default admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Admin::create([
            'name' => 'Admin',
            'email' => 'admin@office.dev',
            'password' => bcrypt('password'),
        ]);

        $this->comment(PHP_EOL.'Created Default Admin!'.PHP_EOL);
    }
}
