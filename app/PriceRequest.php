<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceRequest extends Model
{
    protected $fillable = [
        'vendor_id', 'price_request_no', 'title',
        'price_request_date', 'internal_note',
        'group_id', 'user_id',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeInGroup($query, $groupId)
    {
        return $query->where('group_id', $groupId);
    }

    public function items()
    {
        return $this->hasMany(PriceRequestItem::class);
    }

    public function terms()
    {
        return $this->hasMany(PriceRequestTerm::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }
}
