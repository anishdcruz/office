<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $fillable = [
        'vendor_id', 'purchase_order_no', 'title',
        'purchase_order_date', 'internal_note',
        'in_words', 'group_id', 'user_id',
        'grand_total'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeInGroup($query, $groupId)
    {
        return $query->where('group_id', $groupId);
    }

    public function items()
    {
        return $this->hasMany(PurchaseOrderItem::class);
    }

    public function terms()
    {
        return $this->hasMany(PurchaseOrderTerm::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }
}
