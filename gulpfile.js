var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');
    mix.browserify('invoice.js', 'public/js/invoice.js');
    mix.browserify('price_request.js', 'public/js/price_request.js');
    mix.browserify('purchase_order.js', 'public/js/purchase_order.js');
    mix.browserify('quotation.js', 'public/js/quotation.js');
});
