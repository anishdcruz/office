<?php

return [
    'app' => [
        ['Dashboard', 'group.show'],
        // ['Email', '#'],
        ['break'],
        ['Invoice', 'group.{group}.invoice.index'],
        ['Purchase Order', 'group.{group}.purchase-order.index'],
        ['Price Request', 'group.{group}.price-request.index'],
        ['Quotation', 'group.{group}.quotation.index'],
        ['break'],
        ['Product', 'group.{group}.product.index'],
        ['Client', 'group.{group}.client.index'],
        ['Vendor', 'group.{group}.vendor.index'],
        ['Terms and Condition', 'group.{group}.terms.index'],
        ['break'],
        ['Members', 'group.{group}.member.index'],
        // ['Settings', '#']
    ],
    'admin' => [
        ['Dashboard', 'admin.dashboard'],
        // ['Email', '#'],
        ['break'],
        ['Users', 'admin.admin.user.index'],
        ['Admin', 'admin.admin.admin.index'],
        ['break'],
        ['Settings', 'admin.settings.show']
    ]
];