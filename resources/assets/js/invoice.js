import Vue from 'vue'
import Http from 'vue-resource'
import Convertor from 'number-to-words'
import Capitalize from 'capitalize'

Vue.use(Http);

const token = window.loaded.csrf_token
Vue.http.headers.common['X-CSRF-TOKEN'] = token

const app = new Vue({
  el: 'body',
  data: {
    form: {},
    validation: {},
    clients: window.loaded.clients,
    productSearch: '',
    productSearchResults: [],
    termsSearch: '',
    termsSearchResults: []
  },
  created () {
    Vue.set(this.$data, 'form', window.loaded.form)
    Vue.set(this.form, 'terms', window.loaded.terms)
    Vue.set(this.form, 'products', window.loaded.products)
  },
  methods: {
    addProductLine () {
      this.form.products.push({
        ref_no: '',
        description: '',
        qty: 1,
        unit_price: 0.000
      })
    },
    addProduct (item) {
      this.form.products.push({
        ref_no: item.ref_no,
        description: item.name + ' - ' + item.description,
        qty: 1,
        unit_price: item.unit_price
      })
      this.productSearchResults = []
      this.productSearch = ''
    },
    removeProduct (n) {
      this.form.products.$remove(n)
    },
    beginSearch () {
      this.$http.get('/api/' + window.loaded.group + '/products?query=' + this.productSearch)
        .then(function(response) {
          if(response.data) {
            this.productSearchResults = response.data.products
          }
        })
    },
    addTermsLine () {
      this.form.terms.push({
        description: ''
      })
    },
    removeTerms (term) {
      this.form.terms.$remove(term)
    },
    addTerms (item) {
      this.form.terms.push({
        description: item.description
      })
      this.termsSearchResults = []
      this.termsSearch = ''
    },
    beginTermsSearch () {
      this.$http.get('/api/' + window.loaded.group + '/terms?query=' + this.termsSearch)
        .then(function(response) {
          if(response.data) {
            this.termsSearchResults = response.data.terms
          }
        })
    },
    create () {
      this.$http.post('/group/' + window.loaded.group + '/invoice', this.form)
        .then(function(response) {
          if(response.data.created) {
            window.location = '/group/' + window.loaded.group + '/invoice/' + response.data.id
          }
        })
        .catch(function(response) {
          this.$set('validation', response.data)
        })
    },
    update () {
      this.$http.put('/group/' + window.loaded.group + '/invoice/' + this.form.id, this.form)
        .then(function(response) {
          if(response.data.updated) {
            window.location = '/group/' + window.loaded.group + '/invoice/' + response.data.id
          }
        })
        .catch(function(response) {
          this.$set('validation', response.data)
        })
    }
  },
  computed: {
    subTotal () {
      var total = this.form.products.reduce(function(carry, cv) {
        return carry + (parseFloat(cv.unit_price) * parseFloat(cv.qty));
      }, 0);
      return total
    },
    grandTotal () {
      return this.subTotal - parseFloat(this.form.discount);
    },
    inWords () {
      return Capitalize.words(Convertor.toWords(this.grandTotal)) + ' Kuwaiti Dinar';
    }
  }
})