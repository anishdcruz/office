@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Price Request</p>
            <a href="{{route('group.{group}.price-request.create', [$group])}}" class="btn btn-success box-btn">Create new</a>
        </div>
        <div class="box-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Price Request No.</th>
                        <th>Price Request Date</th>
                        <th>Vendor</th>
                        <th colspan="2">Created At</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($priceRequests as $priceRequest)
                        <tr>
                            <td>{{$priceRequest->price_request_no}}</td>
                            <td>{{$priceRequest->price_request_date}}</td>
                            <td>{{$priceRequest->vendor->company}}</td>
                            <td>{{$priceRequest->created_at}}</td>
                            <td class="table-actions">
                                <a href="{{route('group.{group}.price-request.show', [$group, $priceRequest])}}" class="btn btn-default btn-sm">View</a>
                                <a href="{{route('group.{group}.price-request.edit', [$group, $priceRequest])}}" class="btn btn-primary btn-sm">Edit</a>
                                <form class="form-inline" action="{{route('group.{group}.price-request.destroy', [$group, $priceRequest])}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$priceRequests->render()}}
        </div>
    </div>
@stop