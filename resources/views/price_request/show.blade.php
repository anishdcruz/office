@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">View Price Request</p>
            <div class="btn-group box-btn">
                <a href="{{route('group.{group}.price-request.index', [$group])}}" class="btn btn-default box-btn">Back</a>
                <a href="{{route('group.{group}.price-request.edit', [$group, $priceRequest])}}" class="btn btn-primary box-btn">Edit</a>
                <form class="form-inline" action="{{route('group.{group}.price-request.destroy', [$group, $priceRequest])}}" method="post" onsubmit="return confirm('Are you sure?')">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Delete" class="btn btn-danger">
                </form>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>To</label>
                        <pre class="pre">{{$priceRequest->vendor->person}}<br>{{$priceRequest->vendor->company}}<br>{{$priceRequest->vendor->address}}</pre>
                    </div>
                </div>
                <div class="col-sm-8 text-right">
                    <p><strong>{{$priceRequest->title}}</strong></p>
                    <p>
                        <label>Price Request No:</label>
                        <span> {{$priceRequest->price_request_no}}</span>
                    </p>
                    <p>
                        <label>Date:</label>
                        <span> {{$priceRequest->price_request_date}}</span>
                    </p>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Ref. No</th>
                                <th>Item / Description</th>
                                <th>Qty</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($priceRequest->items as $item)
                                <tr>
                                    <td>{{$item->ref_no}}</td>
                                    <td>{{$item->description}}</td>
                                    <td>{{$item->qty}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <ol>
                        @foreach($priceRequest->terms as $term)
                            <li>{{$term->description}}</li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
@stop