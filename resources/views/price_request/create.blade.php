@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Create Invoice</p>
            <a href="{{route('group.{group}.invoice.index', [$group])}}" class="btn btn-default box-btn">Back</a>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Vendor</label>
                        <select class="form-control" v-model="form.vendor_id">
                            <option value="0">Select</option>
                            <option v-for="vendor in vendors" v-bind:value="vendor.id">
                                @{{vendor.company}}
                            </option>
                        </select>
                        <p v-if="validation.vendor_id" class="text-danger">@{{validation.vendor_id[0]}}</p>
                    </div>
                    <div class="form-group">
                        <label>Price Request No.</label>
                        <input type="text" class="form-control" v-model="form.price_request_no" placeholder="Auto Generated">
                        <p v-if="validation.price_request_no" class="text-danger">@{{validation.price_request_no[0]}}</p>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" v-model="form.title">
                        <p v-if="validation.title" class="text-danger">@{{validation.title[0]}}</p>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Price Request Date</label>
                            <input type="date" class="form-control" v-model="form.price_request_date">
                            <p v-if="validation.price_request_date" class="text-danger">@{{validation.price_request_date[0]}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Internal Note</label>
                        <textarea class="form-control" v-model="form.internal_note"></textarea>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-5">
                    <div class="fussy-search-wrap">
                        <input type="text" class="form-control"
                            placeholder="Search Product"
                            v-model="productSearch"
                            @keyup="beginSearch">
                        <ul class="fussy-search-result">
                            <li v-for="item in productSearchResults">
                                <p @click="addProduct(item)" class="fussy-search-item">
                                    @{{item.name}}
                                    <span class="glyphicon glyphicon-circle-arrow-right"></span>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="table-new_line">
                        <span>OR</span>
                        <span class="btn btn-link" @click="addProductLine">Add New Line</span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped table-bordered">
                        <thead class="table-head">
                            <tr>
                                <th>Ref. No.</th>
                                <th>Item / Description</th>
                                <th>Qty</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody class="table-row">
                            <tr v-for="item in form.products">
                                <td class="table-ref"
                                    :class="{'table-error': validation['products.' + $index +'.ref_no']}">
                                    <input type="text" class="table-input" v-model="item.ref_no">
                                </td>
                                <td class="table-desc"
                                    :class="{'table-error': validation['products.' + $index +'.description']}">
                                    <textarea class="table-text" v-model="item.description"></textarea>
                                </td>
                                <td class="table-qty"
                                    :class="{'table-error': validation['products.' + $index +'.qty']}">
                                    <input type="text" class="table-input" v-model="item.qty">
                                </td>
                                <td class="table-remove">
                                    <strong class="table-times" @click="removeProduct(item)">&times;</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-5">
                    <div class="fussy-search-wrap">
                        <input type="text" class="form-control"
                            placeholder="Search Terms and Condition"
                            v-model="termsSearch"
                            @keyup="beginTermsSearch">
                        <ul class="fussy-search-result">
                            <li v-for="item in termsSearchResults">
                                <p @click="addTerms(item)" class="fussy-search-item">
                                    @{{item.name}}
                                    <span class="glyphicon glyphicon-circle-arrow-right"></span>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="table-new_line">
                        <span>OR</span>
                        <span class="btn btn-link" @click="addTermsLine">Add New Line</span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped table-bordered">
                        <thead class="table-head">
                            <tr>
                                <th colspan="2">Terms and Conditions</th>
                            </tr>
                        </thead>
                        <tbody class="table-row">
                            <tr v-for="item in form.terms">
                                <td class="table-desc"
                                    :class="{'table-error': validation['terms.' + $index +'.description']}">
                                    <textarea class="table-text" v-model="item.description"></textarea>
                                </td>
                                <td class="table-remove">
                                    <strong class="table-times" @click="removeTerms(item)">&times;</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-footer">
                        <input type="submit" value="CREATE" class="btn btn-primary" @click="create">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        window.loaded = {
            group: {{$group->id}},
            vendors: {!! $vendors->toJson() !!},
            terms: [{
                description: ''
            }],
            products: [{
                ref_no: '',
                description: '',
                qty: 1,
            }],
            form: {!! $priceRequest->toJson() !!},
            csrf_token: "{{csrf_token()}}"
        };
    </script>
    <script src="{{asset('js/price_request.js')}}"></script>
@stop