@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Product</p>
            <a href="{{route('group.{group}.product.create', [$group])}}" class="btn btn-success box-btn">Create new</a>
        </div>
        <div class="box-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Ref. No.</th>
                        <th>Name</th>
                        <th colspan="2">Unit Price</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{$product->ref_no}}</td>
                            <td>{{$product->name}}</td>
                            <td>KWD {{$product->unit_price}}</td>
                            <td class="table-actions">
                                <a href="{{route('group.{group}.product.show', [$group, $product])}}" class="btn btn-default btn-sm">View</a>
                                <a href="{{route('group.{group}.product.edit', [$group, $product])}}" class="btn btn-primary btn-sm">Edit</a>
                                <form class="form-inline" action="{{route('group.{group}.product.destroy', [$group, $product])}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$products->render()}}
        </div>
    </div>
@stop