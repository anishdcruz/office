@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Create Product</p>
            <a href="{{route('group.{group}.product.index', [$group])}}" class="btn btn-default box-btn">Back</a>
        </div>
        <div class="box-body">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="row" action="{{route('group.{group}.product.store', [$group->id])}}" method="post">
                {{csrf_field()}}
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" value="{{old('name')}}">
                    </div>
                    <div class="form-group">
                        <label>Unit Price</label>
                        <div class="input-group">
                            <span class="input-group-addon">KWD</span>
                            <input type="text" class="form-control" name="unit_price" value="{{old('unit_price')}}">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Ref. No.</label>
                        <input type="text" class="form-control" name="ref_no" value="{{old('ref_no')}}">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="description">{{old('description')}}</textarea>
                    </div>
                </div>
                <div class="col-sm-12">
                    <input type="submit" value="CREATE" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
@stop

@section('scripts')
@stop