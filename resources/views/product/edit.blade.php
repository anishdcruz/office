@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Update Product</p>
            <a href="{{route('group.{group}.product.index', [$group])}}" class="btn btn-default box-btn">Back</a>
        </div>
        <div class="box-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="row" action="{{route('group.{group}.product.update', [$group->id, $product])}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="put">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" value="{{old('name', $product->name)}}">
                    </div>
                    <div class="form-group">
                        <label>Unit Price</label>
                        <div class="input-group">
                            <span class="input-group-addon">KWD</span>
                            <input type="text" class="form-control" name="unit_price" value="{{old('unit_price', $product->unit_price)}}">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Ref. No.</label>
                        <input type="text" class="form-control" name="ref_no" value="{{old('ref_no', $product->ref_no)}}">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="description">{{old('description', $product->description)}}</textarea>
                    </div>
                </div>
                <div class="col-sm-12">
                    <input type="submit" value="UPDATE" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
@stop

@section('scripts')
@stop