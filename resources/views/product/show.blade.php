@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">View Product</p>
            <div class="btn-group box-btn">
                <a href="{{route('group.{group}.product.index', [$group])}}" class="btn btn-default box-btn">Back</a>
                <a href="{{route('group.{group}.product.edit', [$group, $product])}}" class="btn btn-primary box-btn">Edit</a>
                <form class="form-inline" action="{{route('group.{group}.product.destroy', [$group, $product])}}" method="post" onsubmit="return confirm('Are you sure?')">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Delete" class="btn btn-danger">
                </form>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name</label>
                        <p>{{$product->name}}</p>
                    </div>
                    <div class="form-group">
                        <label>Unit Price</label>
                        <p>KWD {{$product->unit_price}}</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Ref. No.</label>
                        <p>{{$product->ref_no}}</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description</label>
                        <pre class="pre">{{$product->description}}</pre>
                    </div>
                </div>
                <div class="col-sm-12">
                    <small>Created {{$product->created_at->diffForHumans()}} by {{$product->owner->name}}</small>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
@stop