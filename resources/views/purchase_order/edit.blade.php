@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Update Purchase Order</p>
            <a href="{{route('group.{group}.purchase-order.index', [$group])}}" class="btn btn-default box-btn">Back</a>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Vendor</label>
                        <select class="form-control" v-model="form.vendor_id">
                            <option value="0">Select</option>
                            <option v-for="vendor in vendors" v-bind:value="vendor.id">
                                @{{vendor.company}}
                            </option>
                        </select>
                        <p v-if="validation.vendor_id" class="text-danger">@{{validation.vendor_id[0]}}</p>
                    </div>
                    <div class="form-group">
                        <label>Purchase Order No.</label>
                        <input type="text" class="form-control" v-model="form.purchase_order" placeholder="Auto Generated">
                        <p v-if="validation.purchase_order" class="text-danger">@{{validation.purchase_order[0]}}</p>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" v-model="form.title">
                        <p v-if="validation.title" class="text-danger">@{{validation.title[0]}}</p>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Purchase Order Date</label>
                            <input type="date" class="form-control" v-model="form.purchase_order_date">
                            <p v-if="validation.purchase_order_date" class="text-danger">@{{validation.purchase_order_date[0]}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Internal Note</label>
                        <textarea class="form-control" v-model="form.internal_note"></textarea>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-5">
                    <div class="fussy-search-wrap">
                        <input type="text" class="form-control"
                            placeholder="Search Product"
                            v-model="productSearch"
                            @keyup="beginSearch">
                        <ul class="fussy-search-result">
                            <li v-for="item in productSearchResults">
                                <p @click="addProduct(item)" class="fussy-search-item">
                                    @{{item.name}}
                                    <span class="glyphicon glyphicon-circle-arrow-right"></span>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="table-new_line">
                        <span>OR</span>
                        <span class="btn btn-link" @click="addProductLine">Add New Line</span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped table-bordered">
                        <thead class="table-head">
                            <tr>
                                <th>Ref. No.</th>
                                <th>Item / Description</th>
                                <th>Unit Price</th>
                                <th>Qty</th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody class="table-row">
                            <tr v-for="item in form.products">
                                <td class="table-ref"
                                    :class="{'table-error': validation['products.' + $index +'.ref_no']}">
                                    <input type="text" class="table-input" v-model="item.ref_no">
                                </td>
                                <td class="table-desc"
                                    :class="{'table-error': validation['products.' + $index +'.description']}">
                                    <textarea class="table-text" v-model="item.description"></textarea>
                                </td>
                                <td class="table-unit_price"
                                    :class="{'table-error': validation['products.' + $index +'.unit_price']}">
                                    <input type="text" class="table-input" v-model="item.unit_price">
                                </td>
                                <td class="table-qty"
                                    :class="{'table-error': validation['products.' + $index +'.qty']}">
                                    <input type="text" class="table-input" v-model="item.qty">
                                </td>
                                <td class="table-total">
                                    <p class="table-total_text">@{{item.qty * item.unit_price}}</p>
                                </td>
                                <td class="table-remove">
                                    <strong class="table-times" @click="removeProduct(item)">&times;</strong>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot class="table-row">
                            <tr>
                                <td class="table-e" colspan="3"></td>
                                <td class="table-total">
                                    <p class="table-total_text">Grand Total</p>
                                </td>
                                <td class="table-total">
                                    <p class="table-total_text">@{{grandTotal}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="table-e" colspan="3"></td>
                                <td colspan="2">
                                    <textarea class="form-control in-words" v-model="form.in_words" :value="inWords"></textarea>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-5">
                    <div class="fussy-search-wrap">
                        <input type="text" class="form-control"
                            placeholder="Search Terms and Condition"
                            v-model="termsSearch"
                            @keyup="beginTermsSearch">
                        <ul class="fussy-search-result">
                            <li v-for="item in termsSearchResults">
                                <p @click="addTerms(item)" class="fussy-search-item">
                                    @{{item.name}}
                                    <span class="glyphicon glyphicon-circle-arrow-right"></span>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="table-new_line">
                        <span>OR</span>
                        <span class="btn btn-link" @click="addTermsLine">Add New Line</span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped table-bordered">
                        <thead class="table-head">
                            <tr>
                                <th colspan="2">Terms and Conditions</th>
                            </tr>
                        </thead>
                        <tbody class="table-row">
                            <tr v-for="item in form.terms">
                                <td class="table-desc"
                                    :class="{'table-error': validation['terms.' + $index +'.description']}">
                                    <textarea class="table-text" v-model="item.description"></textarea>
                                </td>
                                <td class="table-remove">
                                    <strong class="table-times" @click="removeTerms(item)">&times;</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-footer">
                        <input type="submit" value="UPDATE" class="btn btn-primary" @click="update">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        window.loaded = {
            group: {{$group->id}},
            vendors: {!! $vendors->toJson() !!},
            terms: {!! $purchaseOrder->terms()->get(['description'])->toJson() !!},
            products: {!! $purchaseOrder->items()->get(['ref_no', 'description', 'qty', 'unit_price'])->toJson() !!},
            form: {!! $purchaseOrder->toJson() !!},
            csrf_token: "{{csrf_token()}}"
        };
    </script>
    <script src="{{asset('js/purchase_order.js')}}"></script>
@stop