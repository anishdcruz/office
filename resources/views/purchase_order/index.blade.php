@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Purchase Order</p>
            <a href="{{route('group.{group}.purchase-order.create', [$group])}}" class="btn btn-success box-btn">Create new</a>
        </div>
        <div class="box-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Purchase Order No.</th>
                        <th>Purchase Order Date</th>
                        <th>Vendor</th>
                        <th>Grand Total</th>
                        <th colspan="2">Created At</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($purchaseOrders as $purchaseOrder)
                        <tr>
                            <td>{{$purchaseOrder->purchase_order_no}}</td>
                            <td>{{$purchaseOrder->purchase_order_date}}</td>
                            <td>{{$purchaseOrder->vendor->company}}</td>
                            <td>KWD {{$purchaseOrder->grand_total}}</td>
                            <td>{{$purchaseOrder->created_at}}</td>
                            <td class="table-actions">
                                <a href="{{route('group.{group}.purchase-order.show', [$group, $purchaseOrder])}}" class="btn btn-default btn-sm">View</a>
                                <a href="{{route('group.{group}.purchase-order.edit', [$group, $purchaseOrder])}}" class="btn btn-primary btn-sm">Edit</a>
                                <form class="form-inline" action="{{route('group.{group}.purchase-order.destroy', [$group, $purchaseOrder])}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$purchaseOrders->render()}}
        </div>
    </div>
@stop