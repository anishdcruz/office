@extends('layouts.public')

@section('content')
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
            <div class="box form-login">
                <div class="box-heading">
                    <span>Register</span>
                    <span class="box-btn">
                        <i class="glyphicon glyphicon-user"></i>
                    </span>
                </div>
                <div class="box-body">
                    <form action="/register" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="invitation" value="{{$invite->token}}">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="{{old('name')}}">
                            @if($errors->has('name'))
                                <p class="text-danger">{{$errors->first('name')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="text" name="email" class="form-control" value="{{old('email')}}">
                            @if($errors->has('email'))
                                <p class="text-danger">{{$errors->first('email')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control">
                            @if($errors->has('password'))
                                <p class="text-danger">{{$errors->first('password')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" name="password_confirmation" class="form-control">
                            @if($errors->has('password_confirmation'))
                                <p class="text-danger">{{$errors->first('password_confirmation')}}</p>
                            @endif
                        </div>
                          <button type="submit" class="btn btn-primary">Register</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop