@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Groups</p>
            <p class="box-btn">
                <span class="box-title">
                    {{$group->name}}
                    @if($group->owner_id == Auth::id())
                        <small class="label label-success">You</small>
                    @else
                        <small class="label label-primary">{{$group->owner->name}}</small>
                    @endif
                </span>
            </span>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-3">
                    <div class="dash-box">
                        <h1>{{App\Product::inGroup($group->id)->count()}}</h1>
                        <p>Products</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="dash-box">
                        <h1>{{App\Client::inGroup($group->id)->count()}}</h1>
                        <p>Client</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="dash-box">
                        <h1>{{App\Vendor::inGroup($group->id)->count()}}</h1>
                        <p>Vendors</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="dash-box">
                        <h1>{{App\GroupMember::inGroup($group->id)->count()}}</h1>
                        <p>Members</p>
                    </div>
                </div>
            </div>
            <h4>Group Log</h4>
            <table class="table table-striped">
                <tbody>
                    @foreach($actions as $action)
                        <tr>
                            <td>
                                <span>{{$action->user->name}} {{$action->description}}</span>
                                <span class="pull-right">{{$action->created_at->diffForHumans()}}</span>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop