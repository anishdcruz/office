@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-heading">
                    <p class="box-title">Groups</p>
                    <a href="{{route('group.index')}}" class="btn btn-default box-btn">Back</a>
                </div>
                <div class="box-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="row" action="{{route('group.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="form-group">
                                <label>Group Name</label>
                                <input type="text" class="form-control" name="name" value="{{old('name')}}">
                            </div>
                        </div>
                        <div class="col-sm-4 col-sm-offset-4">
                            <input type="submit" value="CREATE" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop