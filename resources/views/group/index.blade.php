@extends('layouts.master')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Groups</p>
            <a href="{{route('group.create')}}" class="btn btn-success box-btn">Create new</a>
        </div>
        <div class="box-body">
            <div class="row">
                @foreach($groups as $group)
                    <div class="col-sm-3">
                        <div class="group-item">
                            <a class="group-title" href="{{route('group.show', $group->group)}}">
                                {{$group->group->name}}
                            </a>
                            <div class="group-head clearfix">
                                <small class="pull-left">{{$group->group->members->count()}} Members</small>
                                @if($group->group->owner_id == $group->user_id)
                                    <span class="pull-right label label-success" title="Your are the owner">You</span>
                                @else
                                    <span class="pull-right label label-primary" title="{{$group->group->owner->name}} is the owner">{{$group->group->owner->name}}</span>
                                @endif
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop