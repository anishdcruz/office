@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">View Quotation</p>
            <div class="btn-group box-btn">
                <a href="{{route('group.{group}.quotation.index', [$group])}}" class="btn btn-default box-btn">Back</a>
                <a href="{{route('group.{group}.quotation.edit', [$group, $quotation])}}" class="btn btn-primary box-btn">Edit</a>
                <form class="form-inline" action="{{route('group.{group}.quotation.destroy', [$group, $quotation])}}" method="post" onsubmit="return confirm('Are you sure?')">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Delete" class="btn btn-danger">
                </form>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>To</label>
                        <pre class="pre">{{$quotation->client->person}}<br>{{$quotation->client->company}}<br>{{$quotation->client->address}}</pre>
                    </div>
                </div>
                <div class="col-sm-8 text-right">
                    <p><strong>{{$quotation->title}}</strong></p>
                    <p>
                        <label>Quotation No:</label>
                        <span> {{$quotation->quotation_no}}</span>
                    </p>
                    <p>
                        <label>Quotation Date</label>
                        <span> {{$quotation->quotation_date}}</span>
                    </p>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Ref. No</th>
                                <th>Item / Description</th>
                                <th>Unit Price</th>
                                <th>Qty</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($quotation->items as $item)
                                <tr>
                                    <td>{{$item->ref_no}}</td>
                                    <td>{{$item->description}}</td>
                                    <td>{{$item->unit_price}}</td>
                                    <td>{{$item->qty}}</td>
                                    <td>{{$item->total}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot class="table-row">
                            <tr>
                                <td class="table-e" colspan="3"></td>
                                <td class="table-total">
                                    <p class="table-total_text">Sub Total</p>
                                </td>
                                <td class="table-total">
                                    <p class="table-total_text">{{$quotation->sub_total}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="table-e" colspan="3"></td>
                                <td class="table-total">
                                    <p class="table-total_text">Discount</p>
                                </td>
                                <td class="table-total">
                                    <p class="table-total_text">{{$quotation->discount}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="table-e" colspan="3"></td>
                                <td class="table-total">
                                    <p class="table-total_text">Grand Total</p>
                                </td>
                                <td class="table-total">
                                    <p class="table-total_text">{{$quotation->grand_total}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="table-e" colspan="3"></td>
                                <td class="table-total" colspan="2">
                                    <p class="table-in_words text-right">{{$quotation->in_words}}</p>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <ol>
                        @foreach($quotation->terms as $term)
                            <li>{{$term->description}}</li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
@stop