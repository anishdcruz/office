@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Quotation</p>
            <a href="{{route('group.{group}.quotation.create', [$group])}}" class="btn btn-success box-btn">Create new</a>
        </div>
        <div class="box-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Quotation No.</th>
                        <th>Quotation Date</th>
                        <th>Client</th>
                        <th>Grand Total</th>
                        <th colspan="2">Created At</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($quotations as $quotation)
                        <tr>
                            <td>{{$quotation->quotation_no}}</td>
                            <td>{{$quotation->quotation_date}}</td>
                            <td>{{$quotation->client->company}}</td>
                            <td>KWD {{$quotation->grand_total}}</td>
                            <td>{{$quotation->created_at}}</td>
                            <td class="table-actions">
                                <a href="{{route('group.{group}.quotation.show', [$group, $quotation])}}" class="btn btn-default btn-sm">View</a>
                                <a href="{{route('group.{group}.quotation.edit', [$group, $quotation])}}" class="btn btn-primary btn-sm">Edit</a>
                                <form class="form-inline" action="{{route('group.{group}.quotation.destroy', [$group, $quotation])}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$quotations->render()}}
        </div>
    </div>
@stop