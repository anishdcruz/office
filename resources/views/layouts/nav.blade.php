<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/group">Office</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{route('group.index')}}">My Groups</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Hi, {{Auth::user()->name}}</a></li>
                <li><a href="/logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>