<!DOCTYPE html>
<html>
<head>
    <title>Office</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <meta name="viewport" content="initial-scale=1.0">
</head>
<body>
    @include('layouts.nav')
    <div class="container-fluid">
        @if(session()->has('message'))
            <div class="alert alert-info">
                {{session()->get('message')}}
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{session()->get('error')}}
            </div>
        @endif
        @if(session()->has('warning'))
            <div class="alert alert-warning">
                {{session()->get('warning')}}
            </div>
        @endif
        @yield('content')
    </div>
    @yield('scripts')
</body>
</html>