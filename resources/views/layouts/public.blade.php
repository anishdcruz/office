<!DOCTYPE html>
<html>
<head>
    <title>Office</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <meta name="viewport" content="initial-scale=1.0">
</head>
<body>
    <div class="container-fluid">
        @yield('content')
    </div>
</body>
</html>