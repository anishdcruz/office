<!DOCTYPE html>
<html>
<head>
    <title>Office Admin</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <meta name="viewport" content="initial-scale=1.0">
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{route('admin.dashboard')}}">Admin</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Hi, {{Auth::guard('admin')->user()->name}}</a></li>
                    <li><a href="{{route('admin.auth.logout')}}">Logout</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                <div class="sidebar">
                    <ul>
                        @foreach(Config::get('menu.admin') as $item)
                            <li>
                                @if($item[0] == 'break')
                                    <span class="sidebar-break"></span>
                                @else
                                <a href="{{route($item[1])}}">
                                    {{$item[0]}}
                                    <span class="glyphicon glyphicon-circle-arrow-right">
                                </a>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-sm-10">
                @if(session()->has('message'))
                    <div class="alert alert-info">
                        {{session()->get('message')}}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{session()->get('error')}}
                    </div>
                @endif
                @if(session()->has('warning'))
                    <div class="alert alert-warning">
                        {{session()->get('warning')}}
                    </div>
                @endif

                @yield('content')
            </div>
    </div>
    @yield('scripts')
</body>
</html>