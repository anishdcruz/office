<div class="sidebar">
    <ul>
        @foreach(Config::get('menu.app') as $item)
            <li>
                @if($item[0] == 'break')
                    <span class="sidebar-break"></span>
                @else
                <a href="{{route($item[1], $group)}}">
                    {{$item[0]}}
                    <span class="glyphicon glyphicon-circle-arrow-right">
                </a>
                @endif
            </li>
        @endforeach
    </ul>
</div>