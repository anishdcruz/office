Hi!

<br><br>

{{ $group->owner->name }} has invited you to join their group!

<br><br>

Since you already have an account, you are added to their group.

<br><br>

See you soon!

<br>
