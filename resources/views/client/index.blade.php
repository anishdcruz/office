@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Client</p>
            <a href="{{route('group.{group}.client.create', [$group])}}" class="btn btn-success box-btn">Create new</a>
        </div>
        <div class="box-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Company</th>
                        <th>Contact Person</th>
                        <th>Telephone</th>
                        <th colspan="2">Email</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($clients as $client)
                        <tr>
                            <td>{{$client->company}}</td>
                            <td>{{$client->person}}</td>
                            <td>{{$client->telephone}}</td>
                            <td>{{$client->email}}</td>
                            <td class="table-actions">
                                <a href="{{route('group.{group}.client.show', [$group, $client])}}" class="btn btn-default btn-sm">View</a>
                                <a href="{{route('group.{group}.client.edit', [$group, $client])}}" class="btn btn-primary btn-sm">Edit</a>
                                <form class="form-inline" action="{{route('group.{group}.client.destroy', [$group, $client])}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$clients->render()}}
        </div>
    </div>
@stop