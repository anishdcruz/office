@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">View Client</p>
            <div class="btn-group box-btn">
                <a href="{{route('group.{group}.client.index', [$group])}}" class="btn btn-default box-btn">Back</a>
                <a href="{{route('group.{group}.client.edit', [$group, $client])}}" class="btn btn-primary box-btn">Edit</a>
                <form class="form-inline" action="{{route('group.{group}.client.destroy', [$group, $client])}}" method="post" onsubmit="return confirm('Are you sure?')">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Delete" class="btn btn-danger">
                </form>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company</label>
                        <p>{{$client->company}}</p>
                    </div>
                    <div class="form-group">
                        <label>Contact Person</label>
                        <p>{{$client->person}}</p>
                    </div>
                    <div class="form-group">
                        <label>Telephone</label>
                        <p>{{$client->telephone}}</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email</label>
                        <p>{{$client->email or "Not Provided"}}</p>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <pre class="pre">{{$client->address or "Not Provided"}}</pre>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Internal Note</label>
                        <pre class="pre">{{$client->internal_note or "Nil"}}</pre>
                    </div>
                </div>
                <div class="col-sm-12">
                    <small>Created {{$client->created_at->diffForHumans()}} by {{$client->owner->name}}</small>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
@stop