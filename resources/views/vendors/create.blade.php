@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Create Vendor</p>
            <a href="{{route('group.{group}.vendor.index', [$group])}}" class="btn btn-default box-btn">Back</a>
        </div>
        <div class="box-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="row" action="{{route('group.{group}.vendor.store', [$group->id])}}" method="post">
                {{csrf_field()}}
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company <small class="text-info">*</small></label>
                        <input type="text" class="form-control" name="company" value="{{old('company')}}">
                    </div>
                    <div class="form-group">
                        <label>Contact Person<small class="text-info">*</small></label>
                        <input type="text" class="form-control" name="person" value="{{old('person')}}">
                    </div>
                    <div class="form-group">
                        <label>Telephone<small class="text-info">*</small></label>
                        <input type="text" class="form-control" name="telephone" value="{{old('telephone')}}">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" value="{{old('email')}}">
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <textarea class="form-control" name="address">{{old('address')}}</textarea>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Internal Note</label>
                        <textarea class="form-control" name="internal_note">{{old('internal_note')}}</textarea>
                    </div>
                </div>
                <div class="col-sm-12">
                    <input type="submit" value="CREATE" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
@stop

@section('scripts')
@stop