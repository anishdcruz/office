@extends('layouts.app')

@section('content')
<div class="box">
    <div class="box-heading">
        <p class="box-title">Vendor</p>
    <a href="{{route('group.{group}.vendor.create', [$group])}}" class="btn btn-success box-btn">Create new</a>
    </div>
    <div class="box-body">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Company</th>
                    <th>Contact Person</th>
                    <th>Telephone</th>
                    <th colspan="2">Email</th>
                </tr>
            </thead>
            <tbody>
                @foreach($vendors as $vendor)
                    <tr>
                        <td>{{$vendor->company}}</td>
                        <td>{{$vendor->person}}</td>
                        <td>{{$vendor->telephone}}</td>
                        <td>{{$vendor->email}}</td>
                        <td class="table-actions">
                            <a href="{{route('group.{group}.vendor.show', [$group, $vendor])}}" class="btn btn-default btn-sm">View</a>
                            <a href="{{route('group.{group}.vendor.edit', [$group, $vendor])}}" class="btn btn-primary btn-sm">Edit</a>
                            <form class="form-inline" action="{{route('group.{group}.vendor.destroy', [$group, $vendor])}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$vendors->render()}}
    </div>
</div>
@stop