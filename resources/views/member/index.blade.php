@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Members</p>
            @if(Auth::id() == $group->owner_id)
            <a href="{{route('group.{group}.member.create', [$group])}}"
            class="btn btn-success box-btn">Invite People</a>
            @endif
        </div>
        <div class="box-body">
            <h3>Group Members</h3>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email Address</th>
                        <th colspan="2">Joined At</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($group->members as $member)
                        <tr>
                            <td>
                                {{$member->user->name}}
                                @if($group->owner_id == $member->user_id)
                                    @if(Auth::id() == $member->user_id)
                                        <span class="label label-success">You</span>
                                    @else
                                        <span class="label label-primary">Owner</span>
                                    @endif
                                @endif
                            </td>
                            <td>{{$member->user->email}}</td>
                            <td>{{$member->created_at}}</td>
                            <td>
                                @if(Auth::id() == $group->owner_id)
                                    <form class="form-inline"
                                        action="{{route('group.{group}.member.destroy', [$group, $member])}}"
                                        method="post" onsubmit="return confirm('Are you sure?')">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" value="Remove" class="btn btn-danger">
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop