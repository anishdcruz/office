@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Terms</p>
        <a href="{{route('group.{group}.terms.create', [$group])}}" class="btn btn-success box-btn">Create new</a>
        </div>
        <div class="box-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>name</th>
                        <th colspan="2">Description</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($termss as $terms)
                        <tr>
                            <td>{{$terms->name}}</td>
                            <td>{{$terms->description}}</td>
                            <td class="table-actions">
                                <a href="{{route('group.{group}.terms.show', [$group, $terms])}}" class="btn btn-default btn-sm">View</a>
                                <a href="{{route('group.{group}.terms.edit', [$group, $terms])}}" class="btn btn-primary btn-sm">Edit</a>
                                <form class="form-inline" action="{{route('group.{group}.terms.destroy', [$group, $terms])}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$termss->render()}}
        </div>
    </div>
@stop