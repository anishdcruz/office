@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">View Terms</p>
            <div class="btn-group box-btn">
                <a href="{{route('group.{group}.terms.index', [$group])}}" class="btn btn-default box-btn">Back</a>
                <a href="{{route('group.{group}.terms.edit', [$group, $terms])}}" class="btn btn-primary box-btn">Edit</a>
                <form class="form-inline" action="{{route('group.{group}.terms.destroy', [$group, $terms])}}" method="post" onsubmit="return confirm('Are you sure?')">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" value="Delete" class="btn btn-danger">
                </form>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>name</label>
                        <p>{{$terms->name}}</p>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <label>Description</label>
                        <pre class="pre">{{$terms->description or "Nil"}}</pre>
                    </div>
                </div>
                <div class="col-sm-12">
                    <small>Created {{$terms->created_at->diffForHumans()}} by {{$terms->owner->name}}</small>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
@stop