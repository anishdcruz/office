@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Update Terms</p>
            <a href="{{route('group.{group}.terms.index', [$group])}}" class="btn btn-default box-btn">Back</a>
        </div>
        <div class="box-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="row" action="{{route('group.{group}.terms.update', [$group->id, $terms])}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="put">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" value="{{old('name', $terms->name)}}">
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="description">{{old('description', $terms->description)}}</textarea>
                    </div>
                </div>
                <div class="col-sm-12">
                    <input type="submit" value="UPDATE" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
@stop

@section('scripts')
@stop