@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Invoice</p>
            <a href="{{route('group.{group}.invoice.create', [$group])}}" class="btn btn-success box-btn">Create new</a>
        </div>
        <div class="box-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Invoice No.</th>
                        <th>Invoice Date</th>
                        <th>Client</th>
                        <th>Grand Total</th>
                        <th colspan="2">Created At</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($invoices as $invoice)
                        <tr>
                            <td>{{$invoice->invoice_no}}</td>
                            <td>{{$invoice->invoice_date}}</td>
                            <td>{{$invoice->client->company}}</td>
                            <td>KWD {{$invoice->grand_total}}</td>
                            <td>{{$invoice->created_at}}</td>
                            <td class="table-actions">
                                <a href="{{route('group.{group}.invoice.show', [$group, $invoice])}}" class="btn btn-default btn-sm">View</a>
                                <a href="{{route('group.{group}.invoice.edit', [$group, $invoice])}}" class="btn btn-primary btn-sm">Edit</a>
                                <form class="form-inline" action="{{route('group.{group}.invoice.destroy', [$group, $invoice])}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$invoices->render()}}
        </div>
    </div>
@stop