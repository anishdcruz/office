@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Dashboard</p>
        </div>
        <div class="box-body">
            <div class="row">
                @foreach($items as $item)
                    <div class="col-sm-{{$item['size']}}">
                        <div class="dash-box">
                            <h1>{{$item['count']}}</h1>
                            <p>{{$item['title']}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop