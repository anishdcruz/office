@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Create Admin</p>
            <a href="{{route('admin.admin.admin.index')}}" class="btn btn-default box-btn">Back</a>
        </div>
        <div class="box-body">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="row" action="{{route('admin.admin.admin.store')}}" method="post">
                {{csrf_field()}}
                <div class="col-sm-4 col-sm-offset-4">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" value="{{old('name')}}">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" value="{{old('email')}}">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" value="{{old('password')}}">
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" class="form-control" name="password_confirmation" value="{{old('password_confirmation')}}">
                    </div>
                </div>
                <div class="col-sm-4 col-sm-offset-4">
                    <input type="submit" value="SEND" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
@stop

@section('scripts')
@stop