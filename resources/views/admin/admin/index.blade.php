@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Admins</p>
            <a href="{{route('admin.admin.admin.create')}}" class="btn btn-success box-btn">Create new</a>
        </div>
        <div class="box-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th colspan="2">Created At</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($admins as $admin)
                        <tr>
                            <td>{{$admin->name}}</td>
                            <td>{{$admin->email}}</td>
                            <td>{{$admin->created_at}}</td>
                            <td class="table-actions">
                                <a href="{{route('admin.admin.admin.show', [$admin])}}" class="btn btn-default btn-sm">View</a>
                                <form class="form-inline" action="{{route('admin.admin.admin.destroy', [$admin])}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop