@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Settings</p>
            <a href="{{route('admin.admin.user.index')}}" class="btn btn-default box-btn">Back</a>
        </div>
        <div class="box-body">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="row" action="{{route('admin.settings.store')}}" method="post">
                {{csrf_field()}}
                <div class="col-sm-4 col-sm-offset-4">
                    <div class="form-group">
                        <p class="alert alert-warning">You will be logged out after change!</p>
                    </div>
                    <div class="form-group">
                        <label>Current Password</label>
                        <input type="password" class="form-control" name="current_password" value="{{old('current_password')}}">
                    </div>
                    <div class="form-group">
                        <label>New Password</label>
                        <input type="password" class="form-control" name="new_password" value="{{old('new_password')}}">
                    </div>
                    <div class="form-group">
                        <label>Confirm New Password</label>
                        <input type="password" class="form-control" name="new_password_confirmation" value="{{old('new_password_confirmation')}}">
                    </div>
                </div>
                <div class="col-sm-4 col-sm-offset-4">
                    <input type="submit" value="CHANGE" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
@stop

@section('scripts')
@stop