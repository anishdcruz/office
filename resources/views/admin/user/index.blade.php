@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Users</p>
            <a href="{{route('admin.admin.user.create')}}" class="btn btn-success box-btn">Create new</a>
        </div>
        <div class="box-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Type</th>
                        <th colspan="2">Created At</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                @if($user->type == 1)
                                    <span class="label label-success">Regular</span>
                                @else
                                    <span class="label label-info">Basic</span>
                                @endif
                            </td>
                            <td>
                                @if($user->is_banned)
                                    <span class="label label-danger">Banned</span>
                                @else
                                    <span class="label label-success">Active</span>
                                @endif
                            </td>
                            <td>{{$user->created_at}}</td>
                            <td>
                                <a href="{{route('admin.admin.user.show', [$user])}}" class="btn btn-default btn-sm">View</a>
                                <form class="form-inline" action="{{route('admin.admin.user.ban', [$user])}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    @if($user->is_banned)
                                        <input type="submit" value="Un-Ban" class="btn btn-warning btn-sm">
                                    @else
                                        <input type="submit" value="Ban" class="btn btn-warning btn-sm">
                                    @endif
                                </form>
                                <form class="form-inline" action="{{route('admin.admin.user.destroy', [$user])}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop