@extends('layouts.admin')

@section('content')
    <div class="box">
        <div class="box-heading">
            <p class="box-title">Send Invitation</p>
            <a href="{{route('admin.admin.user.index')}}" class="btn btn-default box-btn">Back</a>
        </div>
        <div class="box-body">
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="row" action="{{route('admin.admin.user.store')}}" method="post">
                {{csrf_field()}}
                <div class="col-sm-4 col-sm-offset-4">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" value="{{old('email')}}">
                    </div>
                    <div class="form-group">
                        <label>User Type</label>
                        <select class="form-control" name="user_type">
                            <option>Select</option>
                            <option value="1">Regular</option>
                            <option value="2">Basic</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4 col-sm-offset-4">
                    <input type="submit" value="SEND" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
@stop

@section('scripts')
@stop