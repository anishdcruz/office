@extends('layouts.public')

@section('content')
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
            <div class="box form-login">
                <div class="box-heading">
                    <span>Admin Login</span>
                    <span class="box-btn">
                        <i class="glyphicon glyphicon-lock text-success"></i>
                    </span>
                </div>
                <div class="box-body">
                    <form action="{{route('admin.auth.login')}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="text" name="email" class="form-control" value="{{old('email')}}">
                            @if($errors->has('email'))
                                <p class="text-danger">{{$errors->first('email')}}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control">
                            @if($errors->has('password'))
                                <p class="text-danger">{{$errors->first('password')}}</p>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Log In</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop