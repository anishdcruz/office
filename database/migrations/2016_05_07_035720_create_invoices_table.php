<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->string('title');
            $table->integer('invoice_no');
            $table->date('invoice_date');
            $table->date('due_date');
            $table->text('internal_note');
            $table->decimal('discount', 8, 3);
            $table->decimal('sub_total', 8, 3);
            $table->decimal('grand_total', 8, 3);
            $table->string('in_words');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}
