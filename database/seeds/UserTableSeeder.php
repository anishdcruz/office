<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\User;
use App\Group;
use App\GroupMember;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        User::truncate();
        Group::truncate();

        foreach(range(1, 6) as $i) {
            $user = User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => bcrypt('password'),
                'type' => mt_rand(0, 1)
            ]);

            $group = Group::create([
                'owner_id' => $user->id,
                'name' => $faker->company
            ]);

            GroupMember::create([
                'user_id' => $user->id,
                'group_id' => $group->id
            ]);

            foreach(range(1, mt_rand(2, 6)) as $index) {
                GroupMember::create([
                    'user_id' => mt_rand(1, 6),
                    'group_id' => $group->id
                ]);
            }
        }
    }
}
