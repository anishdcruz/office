# Office App [VIP]

Office App is a web based tool for small business to manage their sales, purchase and teams.

***This is work in progress***
So don't expect things to work properly.

Do not use in production environment (if you want, use it at your own risk).

## Roadmap

* Complete the prototype
* Refactor and write test
* Release 1.0

## System Requirement
1. Laravel Homestead (php 5.6)

## Installation

#### Clone the repo

`git clone http://officeapp.git`

#### Install composer dependencies

`composer install`


#### Install laravel elixir for frontend compilation

`npm install`

#### setup env. file

add a mail driver to send email

#### Install dummy user

`php artisan install:user`

The above command will create a user in database with the following info:

email: `admin@officeapp.dev`

password: `password`

use above credentials to login.

## License

The Office app is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).